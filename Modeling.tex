\begin{figure}[t]
	\centering
	\includegraphics[width=3.34in]{Figures/machining-diagram.pdf}
	\caption{
		A diagram illustrating the geometry and the parameters of the micro-machining process for a single cavity. ``Traj.'' is the tool trajectory; ``S.P.'' is the shear plane.
		}
	\label{fig:machining-diagram}
\end{figure}

\section{Modeling}
\label{sec:modeling}

	In order to better understand the mechanics involved, a micro-machining process may be described using numerical finite-element modeling or semi-analytic theoretical models. Theoretical models are mostly applied to ideal rigid-plastic materials and are not as likely to accurately predict the forces and deformations. Conversely, state of the art numerical models can account for realistic material behavior and friction effects. However, the material properties and tribological behavior of the wax mold material used here have not been sufficiently well characterized to justify a numerical model. Moreover, it is not required to produce a numerical prediction of the cutting forces in terms of the cutting parameters (e.g.\ cutting depth, speed, friction, tool angle, or tip radius) as the forces are, in any case, quite low.

	Instead, it is useful to understand how the cutting parameters and tool geometry affect the deformation behavior. In particular, it is desirable to produce a tightly packed array of cavities in order to obtain a high density of adhesive features. Accordingly, an important question is whether displaced material is moved mainly in the forward direction (towards the unmachined part of the mold) or the rearward  (tending to close up the previously made cavity). Semi-analytic theoretical models can provide this insight without recourse to running numerous simulations, which may be poorly convergent or sensitive to boundary conditions and may require frequent remeshing due to the large deformations involved. 

\subsection{Assumptions.}

	If the cutting depth~$t$ and the tool cross-section are constant along the width of the cavity (into or out of the page in Fig.~\ref{fig:machining-diagram}), and if $t$ is much smaller than the width of the cavity, then the stressed material is confined to a long, narrow prismatic region. In the experiments described here, $t \le 100$~\micron{} and the cavity width is greater than 10~mm, so it is reasonable to assume that the material is in a state of plane strain. 

	The process bears resemblance to two classical problems from plane-strain plasticity theory: oblique wedge indenting~\cite{Hill1947, Hill1950}, in which a rigid wedge-shaped tool penetrates the work surface, and orthogonal machining~\cite{ErnstMerchant1941, Merchant1945, Merchant1945a}, in which the tool removes a thin strip of material by moving parallel to the work surface.  As noted by Johnson \textit{et al.}~\cite{JohnsonSowerbyVenter1982}, some of the proposed solutions for wedge indenting and orthogonal machining show similarities. The process under present consideration is a generalization of the two processes: the tool is wedge-shaped with internal angle~$2\beta$, the centerline of the tool is set at an angle~$\lambda$ with respect to the work surface, and the direction of motion of the tool is neither parallel to the centerline (as in wedge indenting) nor to the work surface (as in orthogonal machining), but instead is set at an intermediate angle $0 < \theta < \lambda$, as shown in Fig.~\ref{fig:machining-diagram}.

	To model this process analytically, it is assumed that the work material is perfectly rigid-plastic. While most plasticity studies have been concerned with the plastic behavior of metals, wax has also been used as a work material~\cite{Bodsworth1957, Bitans1965}, and waxes can be closer to rigid-plastic than metals. The wax used here has been found through axial compression testing to have a low shear yield strength (approximately 2~MPa) and little work hardening; however, it does exhibit some elastic recovery, which can affect the forces and cavity geometry during micro-machining.

	Given these assumptions, it is feasible to adapt an existing semi-analytic model to the present situation to obtain an estimate of the flow of material on both sides of the tool and the expected buildup region adjacent to the cavity. Consider the model proposed by Meguid and Collins~\cite{Meguid1977}, which is an extension of Hill's wedge indenting solution~\cite{Hill1950} to the case where a rigid, frictionless wedge enters the material obliquely. Meguid and Collins assume that the wedge's direction of motion is along its centerline, but their model can be easily modified to allow yawed motion (not parallel to the centerline) by considering the leading and trailing sides of the tool independently as in an earlier model by Hill and Lee~\cite{Hill1946}.

\subsection{Trajectory Angle.}

	For a perfect rigid-plastic material, the interior shape of the cavity will be identical to the swept volume of the tool as it moves along its trajectory, which means that any trajectory angle~$\theta$ can be chosen from the range $\lambda - \beta < \theta < \lambda + \beta$ without affecting the shape of the cavity. However, the extent of plastic deformation and the amount of buildup occurring on the leading and trailing faces of the tool will vary with $\theta$. If material is displaced on both sides of the tool, and the mold cavities are spaced closely, this flow will result in partial collapse of the previously formed cavity.

	In order to minimize this effect, the trajectory angle may instead be chosen to lie outside this range: $\theta = \lambda - \beta - \epsilon$, where $\epsilon > 0$ is a relief angle. This increases the angular width of the cavities by the angle~$\epsilon$.  A depiction of this geometry can be seen in Fig.~\ref{fig:machining-diagram}. The benefit of the relief angle is that the trailing side of the tool should no longer make contact with the wall of the cavity. As a result, assuming that the tip of the tool is sufficiently sharp~\cite{Bitans1965, Li2009}, the zone of plastic deformation is limited to the leading side of the tool only, and material on the trailing side remains rigid throughout the process, theoretically preventing partial collapse of the previous cavity.

\subsection{Plastic Region.}

	The Meguid-Collins model predicts two possibilities for the plastic deformation on the leading side of the tool. In the first case, the plastic region covers the entire area of displaced material, and it is possible to construct a slip-line field throughout this region, similar to the Hill model of wedge indenting~\cite{Hill1950}. In the second case, the plastic region is restricted to a single shear plane, and elsewhere the material is rigid, in a similar manner to the Merchant model of orthogonal machining~\cite{Merchant1945, Merchant1945a}. This second case occurs if the trajectory angle is lower than a critical value:
	\begin{equation}
		2\tan \theta < [1+\tan(\alpha+\theta)]^2
	\end{equation}

	However, this equation is always satisfied if the rake angle~$\alpha$ is positive, as in the present case. Therefore, the model predicts that a single shear plane solution is appropriate on the leading face of the tool. The model also provides a prediction of the shear plane angle~$\phi$ based on an energy-minimization argument similar to the one proposed by Ernst and Merchant~\cite{ErnstMerchant1941}, but since the model does not include friction this prediction is not expected to be accurate. Furthermore, there is doubt about the theoretical and experimental validity of this argument~\cite{JohnsonMellor1973, ChildsRowe1973, Molinari2008}.

\subsection{Cutting Forces.}

	Despite the lack of a trustworthy prediction of the shear plane angle~$\phi$, the model can be used to make a testable prediction about the cutting forces if $\phi$ can be measured experimentally. Let the net force applied by the machine to the tool be denoted by 
	\begin{equation}
		\vec{F} = F_x \hat{x} + F_y \hat{y}
	\end{equation}
and let the total force on the shear plane be denoted by 
	\begin{equation}
		\vec{f} = f_s \hat{s} + f_n \hat{n}
	\end{equation}
as depicted in Fig.~\ref{fig:machining-diagram}. In accordance with the model, it is assumed that the displaced material is limited to a triangular built-up region also shown in Fig.~\ref{fig:machining-diagram}. As long as there is no contact on the trailing side of the tool, these forces are equal: $\vec{F} = \vec{f}$, and therefore:
	\begin{equation}
		f_s = F_x (\hat{x} \cdot \hat{s}) + F_y (\hat{y} \cdot \hat{s}) = F_x \cos \phi + F_y \sin \phi
		\label{eq:projectforce}
	\end{equation}

	This relationship does not require any assumptions about the shear plane angle~$\phi$ or the friction at the leading side of the tool. Finally, according to the theory of perfect rigid-plastic materials in plane strain~\cite{JohnsonMellor1973}, the shear stress along the shear plane is constant and equal to the shear yield stress~$k$:
	\begin{equation}
		f_s / A = k
		\label{eq:yield}
	\end{equation}
where $A$ is the area of the shear plane.

	Although the semi-analytic model cannot be expected to produce a complete prediction of the cutting forces with high accuracy, it produces a useful prediction about the deformation mode of the material (the existence of a shear plane), and it is also useful for understanding relationships among $\lambda$, $\beta$, $\theta$, and $\phi$. This leads to the expectation that most of the displaced material will be pushed forward if the trajectory angle, $\theta$, is sufficiently small compared to the angle of the trailing face of the tool, $\lambda - \beta$. In this situation, the model does produce a testable prediction about the cutting forces (Eqs.~\ref{eq:projectforce} and~\ref{eq:yield}).
