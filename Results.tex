\section{Results}
\label{sec:results}

	Experiments were performed to test the semi-analytic model introduced in Sec.~\ref{sec:modeling}, to empirically characterize the micro-machining process, to characterize the surface roughness of the micro-machined micro-wedges, and to measure the adhesive performance of macroscopic arrays of wedges. These experiments and their results are presented in Secs.~\ref{sec:cutting-force-tests}, \ref{sec:empirical-characterization}, \ref{sec:surface-characterization}, and \ref{sec:adhesive-tests}. However, it is first necessary to look more closely at the geometry of the microtome blades used here as micro-machining tools.

\subsection{Tool Geometry.}
\label{sec:tool-geometry}

	As seen in Fig.~\ref{fig:blade-cross-section}, the blades are not simply a triangular wedge. Instead, the manufacturer has sharpened them to a profile with three different beveled sections. The primary bevel begins approximately 1.7~mm from the tip and has an angle of 12\textdegree{} (this section is above the wax mold at all times). The secondary bevel begins 270~\micron{} from the tip and has an angle of 24\textdegree{}, and the tertiary bevel extends over the final 40~\micron{} of the blade's length and is 34\textdegree{} wide. The tip is too small to be seen at this magnification, but an upper limit radius of 0.9~\micron{} may be established.

	In the described machining geometry, the border between the secondary and tertiary bevels is below the surface of the wax whenever the blade is inserted more than 40~\micron{} deep. However, the mold cavities created by the micro-machining process (with a nominal depth of 100~\micron{}) show little evidence of this border, and the terminal angle of the PDMS wedges is considerably narrower than the tertiary blade bevel. This implies that there is significant elastic behavior occurring in the mold material, as the tips of the mold cavities are narrowing by several degrees when the blade is retracted. This effect is observed for single cavities as well as arrays of cavities.

\begin{figure*}
	\centering
	\includegraphics[width=5.48in]{Figures/cutting-forces.pdf}
	\caption{
		(\emph{a})\ Micrograph of a single mold cavity created in cutting force tests ($\theta=$ 50\textdegree{}, $t=$ 100~\micron{}) showing triangular built-up region.
		(\emph{b})\ Measured shear stress at the shear plane $f_s / A$ versus $\theta$, with values of shear yield stress $k$ for comparison. T, S, and C correspond to trajectories parallel to the tertiary bevel, secondary bevel, and centerline of the tool. The estimated measurement uncertainties of $\theta$ and $f_s / A$ are 0.6\textdegree{} and 0.8~MPa.}
	\label{fig:cutting-forces}
\end{figure*}

\subsection{Cutting Force Tests.}
\label{sec:cutting-force-tests}

	To test the predictions of the semi-analytic model, the cutting forces during micro-machining were measured. A wax specimen of width 1~cm was attached to a six-axis force/torque sensor (ATI Gamma SI-32-2.5) which was mounted in a CNC milling machine, and a variety of micro-machining trajectories were used to create cavities in the wax. The trajectories were linear and differed by trajectory angle, ranging from $\theta=$ 36\textdegree{} to 60\textdegree{}, and maximum depth, ranging from $t=$ 20~\micron{} to 100~\micron{}. The blade centerline angle was $\lambda=$ 60\textdegree{} in all cases, an angle found empirically to produce adhesive features with the desired directional behavior. In this experiment, the cavities were spaced far apart (0.5~mm tip-to-tip) so that the interaction between them was negligible. The blade was wider than the wax specimen so that its corners were not in contact.

	The resulting force data were analyzed to find the cutting force~$\vec{F}$ corresponding to the endpoint of each trajectory, the point in time when the tool was at its maximum cutting depth~$t$ for each cavity. The final cavity shape, preserved by casting PDMS into the specimen, serves as a record of the shape of the cavity at that same point in time. As shown in Fig.~\ref{fig:cutting-forces}a, these castings clearly show the triangular shape of the built-up material adjacent to the leading side of the tool (consistent with the model). By constructing a line from the tip of the cavity to the front edge of the built-up region, and taking into account the width of the wax specimen, it is possible to measure the area of the shear plane~$A$ and the shear plane angle~$\phi$ (Fig.~\ref{fig:cutting-forces}). The $\phi$ measurement has an estimated uncertainty of 4\textdegree{}.

	For each cavity, the value of $\vec{F}$ was projected onto the shear plane using Eq.~\ref{eq:projectforce} to produce an estimate of the shear stress~$f_s / A$. This equation is only accurate if there is no contact between the trailing side of the tool and the wax. If there is such contact, the measured cutting force~$\vec{F}$ will be the sum of forces at the leading and trailing tool faces, which cannot be separated using external measurements.

	The measured values of $f_s / A$ versus $\theta$ are plotted in Fig.~\ref{fig:cutting-forces}b. In addition, the shear yield stress of the wax~$k$ was calculated from the compressive yield stress (which was determined through axial compression testing) by using either the Tresca or von Mises shear yield criterion. The uncertainty in $f_s / A$ is estimated to be 0.8 MPa based on the propagation of measurement uncertainties in $\phi$, $A$, and $\vec{F}$.

	Even though the cutting depth varied from $t=$ 20~\micron{} to 100~\micron{} for each trajectory, causing some variance in the data, the trend is the same for all values of $t$. For trajectories near $\theta = \lambda =$ 60\textdegree{}, there is substantial disagreement between the measurements of $f_s / A$ and the value of $k$, using either the Tresca or von Mises yield criteria. The direction of the cutting force~$\vec{F}$ is nearly \emph{antiparallel} to the shear plane, causing $f_s$ to be negative instead of positive. This may be due to contact forces on the trailing side of the tool because, for trajectories $\theta >$ 43\textdegree{}, it is expected that the secondary or tertiary bevels will contact the wax on the trailing side, due to the blade geometry discussed in Sec.~\ref{sec:tool-geometry}.

	For trajectories $\theta <$ 43\textdegree{}, it is expected that there is no contact on the trailing side of the tool, and therefore the measured value of $f_s / A$ should be equal to $k$ in accordance with Eq.~\ref{eq:yield}. Indeed, the data  for the shallowest trajectory, $\theta=$ 36\textdegree{}, are in agreement with Eq.~\ref{eq:yield}. However, the data for $\theta=$ 42\textdegree{} are not. This disagreement cannot be explained completely by the geometry of the blade. In this case, it is likely that contact is occuring on the trailing side of the blade. This may be due to elastic recovery of the wax (which was assumed negligible in the model) or it may be that the tribological interaction between the tool, lubricant, and mold surface is more complicated than can be described in this simple model.

	In summary, the evidence appears to invalidate the assumption that the material on the trailing side of the tool is rigid, for the majority of the micro-machining trajectories tested. Theoretical modeling has provided useful qualitative insight into the micro-machining process, but the models considered here are unable to explain the actual cutting forces, and they cannot necessarily be used to predict the deformation of the mold material in a process that involves multiple cavities being formed in series. These realizations prompted an empirical investigation of the micro-machining process.

\begin{figure}[tb]
	\centering
	\includegraphics[width=2.67in]{Figures/deformation-micrographs.pdf}
	\caption{
		Micrographs showing the effect of trajectory angle on mold cavity shape. For some trajectories~(\emph{a--b}), a continuous chip of built-up material is formed after the final cavity. The chip disappears for $\theta \ge$ 56\textdegree{}~(\emph{c--d}).
		}
	\label{fig:deformation-micrographs}
\end{figure}

\subsection{Empirical Micro-Machining Characterization.}
\label{sec:empirical-characterization}

	Predicting the cutting force is not strictly necessary to produce a useful adhesive mold insofar as the forces are small enough not to damage or significantly deflect the micro-machining tool. However, it is important to ascertain the effect of the micro-machining trajectory on the shape of the mold cavities. To accomplish this, a characterization experiment was performed in which the trajectory angle was varied (again from $\theta=$ 36\textdegree{} to 60\textdegree{}) while the nominal depth and tip-to-tip spacing of the cavities were kept constant at 100~\micron{} and 60~\micron{} respectively. At this depth and spacing, the cavity shapes were expected to be significantly influenced by neighboring cavities, so a series of ten cavities was made for each trajectory.

	PDMS was cast into the mold cavities and the resulting adhesive samples were cut in cross-section and measured with a microscope, as shown in Fig.~\ref{fig:deformation-micrographs}. Ten cavities appear to be sufficient to attain a steady-state shape; the boundary conditions are different for the initial cavity, but this only affects the first three cavities or fewer. In addition, the final cavity is sometimes a different shape from the previous ones. In these cases, the final cavity shows the shape of an incipient cavity before it has been deformed to its completed shape by the cavity following it. The 4th--9th cavities are representative of the completed shapes that would be created in a large adhesive array.

\begin{figure}[t]
	\centering
	\includegraphics[width=3.34in]{Figures/deformation-vs-trajectory.pdf}
	\caption{
		Geometric data taken from characterization experiment micrographs (Fig.~\ref{fig:deformation-micrographs}). Feature height is measured normal to the mold surface from the tip of the cavity to its upper edge.
		}
	\label{fig:deformation-vs-trajectory}
\end{figure}

	The height and angular width of the cavities change with the trajectory angle due to several concurrent effects. For values of $\theta$ near 36\textdegree{}, the feature height is significantly less than the nominal height of 100~\micron{} because the cavities intersect one another below the original mold surface (Fig.~\ref{fig:deformation-micrographs}a). As $\theta$ increases past 46\textdegree{}, there is an increasingly large difference in height between the incipient feature and the completed features (Fig.~\ref{fig:deformation-micrographs}b), indicating that permanent deformation on the trailing side of the tool is ocurring. The feature height reaches a maximum at $\theta=$ 56\textdegree{}, where the trailing-side deformation causes the edges of the cavities to be raised up above the original mold surface (Fig.~\ref{fig:deformation-micrographs}c). As $\theta$ is increased further to $\theta = \lambda =$ 60\textdegree{}, the features become shorter again and the tip angle diminishes well below the angular width of the blade, indicating that the rearward deformation is causing the cavities to close up at their tips (Fig.~\ref{fig:deformation-micrographs}d). These trends are plotted in Fig.~\ref{fig:deformation-vs-trajectory}.

\subsection{Surface Characterization.}
\label{sec:surface-characterization}

	The surface roughness of adhesive wedges before post-treatment was measured by capturing stereoscopic SEM images and generating 3D topographical plots using the Alicona Imaging MeX software package. Average roughness parameters were computed from 50~\micron{} long line profiles taken across the engaging surfaces of the features. When the micro-machining process was performed using the best lubricant (described in Sec.~\ref{sec:lubrication}), the resulting RMS roughness parameter was $R_q$~= 39~nm. This measurement technique can be sensitive to a number of types of calibration and numerical errors, however~\cite{Marinello2008}.

	Therefore, an independent surface characterization was conducted. Micro-machined PDMS wedges were measured over 2$\times$2~\micron{} and 6$\times$6~\micron{} areas using a Park Systems~XE-70 AFM in non-contact mode after excising the wedges and immobilizing them on double-sided tape. The RMS average roughness for a micro-machined wedge before post-treatment was measured as $R_q$~= 20\,$\pm$\,5~nm for the 2$\times$2~\micron{} scan area and  $R_q$~= 48\,$\pm$\,5~nm for the 6$\times$6~\micron{} scan area. The $R_q$ value was not found to continue to increase with larger scan areas, however. This increasing trend and subsequent limit is a typical result for $R_q$, and is an indication of the maximum horizontal size scale of the surface roughness~\cite{Bhushan1999}. Over these areas, the heights of the tallest asperities were $R_p$~= 61~nm and $R_p$~= 183~nm respectively. This is a very low roughness, comparable to a finely lapped surface~\cite{Bhushan2010}.

	The same measurements were repeated on a micro-machined wedge after post-treatment. For this sample, the RMS roughness parameters were measured as $R_q$~= 18\,$\pm$\,5~nm for the 2$\times$2~\micron{} scan area and  $R_q$~= 50\,$\pm$\,5~nm for the 6$\times$6~\micron{} scan area. These data do not support the hypothesis that there is a difference in the $R_q$ values before and after post-treatment. Alternatively, the heights of the tallest asperities for this sample were $R_p$~= 45~nm and $R_p$~= 157~nm for the two scan areas. This suggests that the post-treatment process may reduce the surface roughness by limiting the heights of the tallest asperities. This would not significantly affect the value of $R_q$ because the tallest asperities make up a small percentage of the total area.

\begin{figure*}[t]
	\centering
	\includegraphics[width=5.48in]{Figures/load-pull-test.pdf}
	\caption{
		(\emph{a}) Diagram of the adhesive testing apparatus, showing the normal and shear directions.
		(\emph{b}) Diagram illustrating the movement of the positioning stage during load-pull tests.
		}
	\label{fig:load-pull-test}
\end{figure*}

\subsection{Adhesive Tests.}
\label{sec:adhesive-tests}

	Samples of micro-machined adhesives were fabricated to test their adhesive properties. 
The blade was held at $\lambda$~= 60\textdegree{} and the trajectory was chosen to be $\theta$~= 48\textdegree{}, an angle approximately parallel to the rear face of the tool, and found empirically to push most of the displaced material forward. The nominal depth and tip-to-tip spacing were 100~\micron{} and 60~\micron{}, as in Sec.~\ref{sec:empirical-characterization}.

	Following the procedure described by Santos \textit{et~al.}~\cite{Santos2007a}, 
adhesion force data were collected on an instrumented stage capable of moving the adhesive samples in and out of contact with a flat glass substrate along a specified trajectory and loading the adhesive in both the normal and shear directions.  The stage (Velmex MAXY4009W2-S4 and MA2506B-S2.5) is capable of 10~\micron{} positioning resolution in the shear direction and 1~\micron{} in the normal direction.  The adhesive samples were mounted on a stationary six-axis force/torque transducer (ATI Gamma SI-32-2.5) with a force measurement resolution of approximately 10~mN. The transducer was mounted on a two-axis goniometer to allow the adhesive and substrate to be precisely aligned. This apparatus is shown in Fig.~\ref{fig:load-pull-test}a.

	A sample of adhesive is tested by bringing it into contact with the substrate along a 45\textdegree{} approach trajectory until the adhesive reaches a certain preload depth. The preload depth is defined as the distance by which the adhesive is pressed into the substrate, measured normal to the substrate, from the position where the tips of the adhesive features make first contact. The preload phase allows for the gradual loading of the features and does not generate slippage. Once the sample is at the appropriate preload depth, it is pulled out of contact along a trajectory at a specified pull-off angle.  Such tests are referred to as load-pull tests (see Fig.~\ref{fig:load-pull-test}b). To obtain the adhesion limit curve~\cite{Santos2007a}, a battery of load-pull tests were performed for preload depths ranging from 30--80~\micron{} and pull-off angles ranging from 0--90\textdegree{}.

\begin{figure*}[t]
	\centering
	\includegraphics[width=4.80in]{Figures/post-treated-limit-curve.pdf}
	\caption{
		Comparison of the limit curves for macroscopic arrays of adhesive micro-wedges produced with micro-machined molds and photolithographic molds. The measurement uncertainty of the data is estimated to be 2~kPa in normal and 1~kPa in shear.
		}
	\label{fig:post-treated-limit-curve}
\end{figure*}

	Limit curves were generated for a patch of micro-machined adhesive both before and after the post-treatment process step (overall sample area 1.21~cm$^2$). For comparison, a limit curve was generated for a patch of photolithographic micro-wedge adhesive (overall sample area 0.37~cm$^2$), consisting of a rectangular pattern of right triangular prisms, approximately 20~\micron{} wide, 80~\micron{} tall, 200~\micron{} long, and with a tip-to-tip spacing of 40~\micron{} between features. These features are pictured in Fig.~\ref{fig:wedges}a--b.

	For the micro-machined adhesive sample, the normal preload pressure applied at the maximum preload depth of 80~\micron{} was approximately 14.6~kPa before post-treatment and 17.8~kPa after post-treatment. For the photolithographic adhesive sample, the preload pressure at 80~\micron{} was approximately 64~kPa. This indicates that the photolithographic micro-wedges have a higher mechanical stiffness.  

	The limit curves show the adhesives' performance in force space.  Each point corresponds to a combination of normal force and shear force at which failure occurred. The region above the curve is the ``safe region'':  Forces above the curve can be sustained by the adhesive; forces below the curve cause it to fail. The adhesive test results are consistent with the directional adhesion model proposed by Autumn \textit{et~al.}\ for geckos~\cite{Autumn2006}, in which adhesion increases with increasing shear force.

	As shown in Fig.~\ref{fig:post-treated-limit-curve}, the photolithographic adhesive produces a maximum adhesive stress of approximately 18~kPa when loaded with a shear stress of approximately 51~kPa, and the micro-machined adhesive with no post-treatment achieves a maximum adhesion of 13~kPa at a shear stress of 37~kPa. After post-treatment, the micro-machined adhesive has a maximum adhesion of 38~kPa at a shear stress of 49~kPa. The measurement uncertainty of these force data is estimated to be 2~kPa in the normal direction and 1~kPa in the shear direction.

	At high levels of shear stress, all of the adhesive samples show a ``roll-off'' in adhesion as increasing numbers of wedges start to slide along the surface.
