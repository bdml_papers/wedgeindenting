% Daniel Soto
% Mon Oct 29 11:44:55 PDT 2007
% <tag> dsoto.matlab </tag>
clear all
clc

close all

data = load ( './test_060um_60deg_t1.txt' ); %insert directory and filename

%Choose to limit the time
time_limit_lo = 1000;
time_limit_hi = 1250;

%Choose Y limits
y_limit = [-1.5 2];


[time dx dy dz ax ay az fx fy fz mx my mz] = getTraces(data);

% Tare force data
fxBias = mean(fx(1:1000));
fyBias = mean(fy(1:1000));
fzBias = mean(fz(1:1000));
fx = fx - fxBias;
fy = fy - fyBias;
fz = fz - fzBias;

% tare actual position data
axBias = mean(ax(1:1000));
ayBias = mean(ay(1:1000));
azBias = mean(az(1:1000));
ax = ax - axBias;
ay = ay - ayBias;
az = az - azBias;

% Filter data
[numd,dend] = butter(3,.2);

fx_filt = filtfilt(numd,dend,fx);
fy_filt = -filtfilt(numd,dend,fy);
fz_filt = -filtfilt(numd,dend,fz);

% fx_filt = fx;
% fy_filt = -fy;
% fz_filt = -fz;


% scale force data for plotting purposes
forceMax = max([fx_filt;fy_filt;fz_filt]);
forceMin = min([fx_filt;fy_filt;fz_filt]);
forceScaleFactor = max(abs(forceMax),abs(forceMin));
% fx_filt = fx_filt / forceScaleFactor; % Want the actual force data
% fy_filt = fy_filt / forceScaleFactor;
% fz_filt = fz_filt / forceScaleFactor;

% scale position data for plotting purposes
positionMax = max([ax;ay;az]);
positionMin = min([ax;ay;az]);
positionScaleFactor = max(abs(positionMax),abs(positionMin));
ax = ax / positionScaleFactor;
ay = ay / positionScaleFactor;
az = az / positionScaleFactor;

% plotting code
axesHandle = gca;
figureHandle = gcf;


%Figure formatting
setpapersize_fn(gcf, [824, 618]); %4:3


axes(axesHandle);
%plot(fx_filt,'k');
hold on;
axis([0 (time_limit_hi - time_limit_lo) y_limit(1) y_limit(2)]);
plot(fy_filt(time_limit_lo:time_limit_hi),'-r','LineWidth',2);
plot(fz_filt(time_limit_lo:time_limit_hi),'--b','LineWidth',2);
%plot(ax,'k','LineWidth',3);
%plot(ay,'r','LineWidth',3);
%plot(az,'b','LineWidth',3);
legendStrings = {'Shear','Normal'};%,'Ax','Ay','Az'};
legend(legendStrings,'FontSize',12,'Location','NorthEast');
%title(axesHandle, 'Time Trace of Force and Position');
xlabel (axesHandle, 'Time (ms)');
ylabel (axesHandle, 'Force (N)');


hold off;
formatPlot( figureHandle, axesHandle, 'Times New Roman', 24 );

%More formatting
setpapersize_fn(gcf, [824, 618]); %4:3
setfiguremargintozero;
eval(['print -dpdf ','./forcetrace.pdf']);
