close all
clear all
clc

filenames = char('IndentPreTreat.mat', 'IndentPostTreat.mat','bestSqueegee.mat');
trendNames = {'Micromachined adhesive', 'Micromachined adhesive (post-treated)','Photolithographic adhesive'};

for file_index = 1 : size(filenames,1)
    
eval(sprintf('load ./%s',filenames(file_index,:)));

nominalPreloadDepths = distancePreloadMicron;
actualPreloadDepths = depthPreloadMeasured;

measuredPreloadFx = FxPreload;
measuredPreloadFy = FyPreload;
measuredPreloadFz = FzPreload;

preloadPressureKPa = -FzPreload ./ areaSampleSquareCentimeter * 10;

figure;
hold on;
for i=1:length(distancePreloadMicron)
	for j=1:length(anglePulloffDegree)
		for k=1:numTrials
            plot(nominalPreloadDepths(i),actualPreloadDepths(i,j,k),'b.');
        end
    end
end

figure;
hold on;
for i=1:length(distancePreloadMicron)
	for j=1:length(anglePulloffDegree)
		for k=1:numTrials
            plot(actualPreloadDepths(i,j,k),preloadPressureKPa(i,j,k),'r.');
        end
    end
end



end
