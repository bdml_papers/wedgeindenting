I identified a couple of data points which should be discarded since the measured preload distance was significantly different than the nominal preload distance:

Data set 'IndentPostTreat'
indices i=7, j=9, k=2 (nominal preload distance 70 microns, nominal pulloff angle 80deg, trial 2)
indices i=7, j=9, k=3 (nominal preload distance 70 microns, nominal pulloff angle 80deg, trial 3)

These data have force values of:
(7,9,2): Shear = 70.8028 kPa, Normal = -5.4507 kPa
(7,9,3): Shear = 70.5719 kPa, Normal = -4.7110 kPa

These data can be deleted from the figure.

I also found that all the outliers in the figure were for preloads 10, 20 and 30 microns. Since we already state that the range of preloads was 30-80 microns, we can delete the 10, 20, and 90 micron data, and it will make the plot look better too. 

However, I have decided to keep the 90 micron data since it covers the crucial high-adhesion area of the curve. In addition, there are some data points near the origin from 10 and 20 microns that are worth keeping too, since they show that the limit curve passes through the origin.

According to the data taken during the experiment, the preload pressure ranges from 30 microns to 80 microns were:

Untreated micromachined 	6.8-14.6 kPa
Post-treated micromachined	7.4-17.8 kPa
Lithographic			33-64 kPa


-Eric