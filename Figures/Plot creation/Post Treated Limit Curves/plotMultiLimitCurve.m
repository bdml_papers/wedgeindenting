% This script will plot multiple, overlaid limit curves with incrementing
% colors and symbols, as well as a legend indicating which trend is which.
% Simply enter the file names of the .mat files produced by the
% parseexperiment.m script (which need to be copied to the local directory)
% into the 'filenames' character array, and the string you'd like to
% describe them with in the 'trendNames' array.  It will also size, crop,
% and print out a .pdf file names multiLimitCurve.pdf to the local
% directory.
%
% Paul Day
% 03/23/2011

close all
clear all
clc

% First plot
%run ./configure.m
filenames = char('IndentPreTreat.mat', 'IndentPostTreat.mat','bestSqueegee.mat');
trendNames = {'Micromachined adhesive', 'Micromachined adhesive (post-treated)','Photolithographic adhesive'};
% filenames = char('bestSqueegee.mat', 'IndentPreTreat.mat'); % PLot w/o post treated trends
% trendNames = {'Untreated SU-8 Molded','Untreated Indented'}; % PLot w/o post treated trends
plotHvec = [];

for file_index = 1 : size(filenames,1)
    
eval(sprintf('load ./%s',filenames(file_index,:)));


FxPulloffVector_mean = mean(FxPulloffVector,3);
FyPulloffVector_mean = mean(FyPulloffVector,3);
FzPulloffVector_mean = mean(FzPulloffVector,3);

FxPulloffVectorKPa = FxPulloffVector ./ areaSampleSquareCentimeter * 10;
FyPulloffVectorKPa = FyPulloffVector ./ areaSampleSquareCentimeter * 10;
FzPulloffVectorKPa = FzPulloffVector ./ areaSampleSquareCentimeter * 10;

% if using corrected preloadDistances
%distancePreloadMicron = distancePreloadMicron - estimateZero;

% generate plot
%figure;
hold on
color = ['b' 'r' 'k' 'g' 'm' 'y' 'c'];
shape = ['o' '^' 'x' '*' 's' 'x' '.'];
size = [10 10 14 10 10 10 10];

minimumPlottedPreload = 30;
maximumPlottedPreload = 80;

plotH = [];

for i=1:length(distancePreloadMicron)
%    if distancePreloadMicron(i) < minimumPlottedPreload || ...
%            distancePreloadMicron(i) > maximumPlottedPreload
%        continue
%    end
	for j=1:length(anglePulloffDegree)
		for k=1:numTrials
			plotH(i,j) = plot(-FyPulloffVectorKPa(i,j,k),...
					         -FzPulloffVectorKPa(i,j,k),...
									 [color(file_index),...
									  shape(file_index)],...
									 'MarkerSize', size(file_index),'LineWidth',2);
		end
	end
end

%Figure formatting
setpapersize_fn(gcf, [824, 500]);

xlabel('Shear Stress  (kPa)');
ylabel('Normal Stress  (kPa)');

plotHvec = [plotHvec plotH(end,1)];

legend(plotHvec, trendNames, ...
				'FontSize', 14, ...
				'Location','NorthWest' );

%xlocations = [12 5 10];
%ylocations = [-3 -35 -15];
            
%slopeString = sprintf('Patch Area = %3.2f cm^2 ', areaSampleSquareCentimeter );
%text(xlocations(file_index),ylocations(file_index),slopeString, 'FontSize', 18);


axis equal;
axis([-5 85 -40 10]);
x = get(gca,'XLim');
y = get(gca,'YLim');
grid on;
%grid minor;
axis(gca,[x,y]);
line(x,[0 0],[0 0],'Color','k','LineWidth',2);
line([0 0],y,[0 0],'Color','k','LineWidth',2);
formatPlot(gcf,gca,'Times New Roman',22);

hold on

end

%More formatting
setpapersize_fn(gcf, [824, 500]);
setfiguremargintozero;
eval(['print -dpdf ','./postTreatedLimitCurve.pdf']);