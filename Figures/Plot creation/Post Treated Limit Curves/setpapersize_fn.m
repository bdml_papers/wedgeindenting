% fighandle is the figure handle, can just input "gcf".
% widthheight is the width and height of the figure we are making; 
% [560 420] is what matlab uses when it plots new figures.
% For example, widthheight = [560 420];
% so, can do e.g.
% setpapersize_fn(gcf, [560 420]);
% 
% or if you only input the first argument, it will use whatever the current
% paper size is!
%
% Also if you input 'default' (or any string, actually) as the second
% argument, it will use [560 420].
%
% So in summary, you can do:
% setpapersize_fn(gcf);
% setpapersize_fn(gcf, [560 420]);
% setpapersize_fn(gcf, 'default');


function setpapersize_fn(fighandle, varargin)
% function setpapersize_fn(fighandle, widthheight)

% I think it gets messed up if it thinks the paper is in landscape mode..
set(fighandle,'PaperOrientation','portrait')

% figure out what the current lower left corner is and keep that the same:
% actually, we want to keep the *upper* left corner the same, so do
% computations to figure out what that is:
currentposition = get(fighandle, 'Position');
lowerleftpos = currentposition(1:2);

% if we only input one argument, determine what the current figure size is
% and keep that.  We will overwrite this later if we need to change the
% widthheight:
widthheight = currentposition(3:4);

upperleftpos = lowerleftpos + [0 widthheight(2)];  



% overwrite widthheight if needed:
if (nargin > 1)
    if (isnumeric(varargin{1}))  % if the user specified a widthheight
        widthheight = varargin{1};
        % set the lowerleftpos to keep the upper left in the same spot:
        lowerleftpos = upperleftpos - [0 widthheight(2)];
        
    else  % 'default' or other text as the second argument
        widthheight = [560 420];
    end
end



% monitor is 96dpi
monitordpi = 96;  



 % this is the default, just re-setting it to make sure
set(fighandle, 'PaperUnits', 'inches'); 

% % width and height of the figure we are making; [560 420] is what matlab
% % uses when it plots new figures.
% widthheight = [560 420];

% set the paper size in inches to be the width and height of the figure
set(fighandle, 'PaperSize', widthheight/monitordpi);  

% set the figure to be the same size as the paper!
% Note this is in pixels, not inches.  Also, the first two entries of the
% position rectangle are arbitrary (here I put 100).
% [left, bottom, width, height]
set(fighandle, 'Position', [lowerleftpos widthheight]);  


% PaperPosition
%     four-element rect vector
%     Location on printed page. A rectangle that determines the location of
%     the figure on the printed page. Specify this rectangle with a vector
%     of the form  
%      rect = [left, bottom, width, height]
% set left, bottom = 0 because we want it to go right to the border of the
% page.
set(fighandle, 'PaperPosition', [0 0 widthheight]);


% PaperPositionMode
%     auto | {manual}
%     WYSIWYG printing of figure. In manual mode, MATLAB honors the value
%     specified by the PaperPosition property. In auto mode, MATLAB prints
%     the figure the same size as it appears on the computer screen,
%     centered on the page.   

% set(fighandle, 'PaperPositionMode', 'manual')
set(fighandle, 'PaperPositionMode', 'auto')

% The default is manual.  Must put it to auto mode if you want to be able
% to save it as a jpeg without running out of memory.  It's ok to set it to
% auto because, when we save it as a pdf, it will use the page size and
% stuff that we already specified (it's already the correct size).  Then
% do, to save it as a jpg: 

% print -djpeg99 -r300 Filename.jpg

% the -r option is the resolution in dpi, set it to monitordpi==96 to make
% it the same as the screen; the default is 150 if you don't specify it; or
% you can make it better e.g. -r300 for 300dpi.

