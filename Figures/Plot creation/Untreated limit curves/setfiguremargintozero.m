% from http://www.mathworks.com/matlabcentral/newsreader/view_thread/156326
% There is an undocumented axes property LooseInset which
% seems to determine the amount of space outside the axes. I
% think that the command
% 
% set(gca,'LooseInset',get(gca,'TightInset'))
% 
% will remove the whitespace whithout modifying the
% OuterPostion and ActivePositionProperty. 
%
% ==> you have to run this every time you resize the figure, but it works!

set(gca,'LooseInset',get(gca,'TightInset'))


% all these vectors are:
% [leftedge loweredge width height]

% axis Position = [63.775 51.4 761.67 336] in pixels  or 
% Position = [0.075 0.12 0.91 0.8]  in normalized units
% 
% also
% OuterPosition = [-0.0776452 -0.00857143 1.17419 1.01429] in normalized units
% OuterPosition = [-63.989 -2.6 982.8 426] in pixels
% 
% TightInset = [0.0824373 0.128571 0.0155317 0.0857143] in normalized units
% TightInset = [69 54 13 36]  in pixels



% OuterPosition � The boundary of the axes including the axis labels,
% title, AND A MARGIN. For figures with only one axes, this is the interior
% of the figure.

% Position � The boundary of the axes, excluding the tick marks and labels,
% title, and axis labels. 

% ActivePositionProperty � Specifies whether to use the OuterPosition or
% the Position property as the size to preserve when resizing the figure
% containing the axes.  

% TightInset � The margins added to the width and height of the Position
% property to include text labels, title, and axis labels. 

% Position + TightInset is the outer boundary around the title, text
% labels, etc.  I'm not sure what units TightInset is in.


% figure Position = [12 71 837 420]  in pixels (default)


% So.... if we want to make the figure have zero boundary around the text
% labels, etc, we need to set the Position to be the size of (available
% area) minus TightInset.

% As you resize the figure, MATLAB maintains the area defined by the
% TightInset + Position so the text is not cut off. Compare the next two
% graphs, which have both been resized to the same figure size.
% ==> should set ActivePositionProperty = OuterPosition to keep it from
% cutting off the text.
% set(gca,'ActivePositionProperty','OuterPosition')

% pos = get(gca,'position')
% tightinset = get(gca,'TightInset')
% 
% posplustightinset = zeros(4,1);
% posplustightinset(1) = pos(1) - tightinset(1);
% posplustightinset(2) = pos(2) - tightinset(2);
% posplustightinset(3) = pos(3) + tightinset(3);
% posplustightinset(4) = pos(4) + tightinset(4);
% 
%%%%%% This part below doesn't quite work yet, use the command at the very
%%%%%% top to do what you want!
% newpos = zeros(4,1);
% newpos(1) = tightinset(1);
% newpos(2) = tightinset(2);
% newpos(3) = pos(3) + tightinset(3);
% newpos(4) = pos(4) + tightinset(4);
% % newpos(3) = pos(3) + tightinset(3);
% % newpos(4) = pos(4) + tightinset(4);
% 
% set(gca,'Position',newpos);
