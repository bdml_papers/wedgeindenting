% This script will plot multiple, overlaid limit curves with incrementing
% colors and symbols, as well as a legend indicating which trend is which.
% Simply enter the file names of the .mat files produced by the
% parseexperiment.m script (which need to be copied to the local directory)
% into the 'filenames' character array, and the string you'd like to
% describe them with in the 'trendNames' array.  It will also size, crop,
% and print out a .pdf file names multiLimitCurve.pdf to the local
% directory.
%
% Paul Day
% 03/23/2011

close all
clear all
clc

% First plot
%run ./configure.m
filenames = char('bestStandard.mat', 'bestModified.mat', 'bestSqueegee.mat');
trendNames = {'Standard Indenting Method','Modified Indenting Method', 'SU-8 Molded'};
plotHvec = [];

for file_index = 1 : size(filenames,1)
    
eval(sprintf('load ./%s',filenames(file_index,:)));


FxPulloffVector_mean = mean(FxPulloffVector,3);
FyPulloffVector_mean = mean(FyPulloffVector,3);
FzPulloffVector_mean = mean(FzPulloffVector,3);

FxPulloffVectorKPa = FxPulloffVector ./ areaSampleSquareCentimeter * 10;
FyPulloffVectorKPa = FyPulloffVector ./ areaSampleSquareCentimeter * 10;
FzPulloffVectorKPa = FzPulloffVector ./ areaSampleSquareCentimeter * 10;

% if using corrected preloadDistances
%distancePreloadMicron = distancePreloadMicron - estimateZero;

% generate plot
%figure;
hold on
color = ['b' 'g' 'r' 'c' 'm' 'y' 'k'];
shape = ['o' 'x' '+' '*' 's' 'd' 'v' 'p' 'h' '.'];
for i=1:length(distancePreloadMicron)
	for j=1:length(anglePulloffDegree)
		for k=1:numTrials
			plotH(i,j) = plot(-FyPulloffVectorKPa(i,j,k),...
					         -FzPulloffVectorKPa(i,j,k),...
									 [color(file_index),...
									  shape(file_index)],...
									 'MarkerSize', 10,'LineWidth',2);
		end
	end
end

%Figure formatting
setpapersize_fn(gcf, [824, 618]); %4:3

xlabel('Shear Pressure (kPa)');
ylabel('Normal Pressure (kPa)');

plotHvec = [plotHvec plotH(i,1)];

legend(plotHvec, trendNames, ...
				'FontSize', 12, ...
				'Location','NorthEast' );

slopeString = sprintf('Patch Area = %3.3f cm^2 ', areaSampleSquareCentimeter );
text(10,40,slopeString, 'FontSize', 18);


axis([-10 80 -20 10]);
x = get(gca,'XLim');
y = get(gca,'YLim');
grid on;
%grid minor;
axis(gca,[x,y]);
line(x,[0 0],[0 0],'Color','k','LineWidth',2);
line([0 0],y,[0 0],'Color','k','LineWidth',2);
formatPlot(gcf,gca,'Times New Roman',24);

hold on

end

%More formatting
setpapersize_fn(gcf, [824, 618]); %4:3
setfiguremargintozero;
eval(['print -dpdf ','./untreatedLimitCurve.pdf']);