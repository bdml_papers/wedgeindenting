function [time dx dy dz ax ay az fx fy fz mx my mz] = get_traces ( data );
% data file is in 
% col  1 time 
% col  2 desired x pos
% col  3 desired y pos
% col  4 desired z pos
% col  5 actual x pos
% col  6 actual y pos
% col  7 actual z pos
% col  8 force plate x
% col  9 force plate y
% col 10 force plate z
% col 11
% col 12
% col 13

data_length = size ( data, 1 );
data_start = 1;
data_end = data_length;

% Extract data

time =  data(data_start:data_end,1);

% desired trajectories
dx   = -data(data_start:data_end,2);  % +x 
dy   = -data(data_start:data_end,3);  % +y away from the wall
dz   =  data(data_start:data_end,4);  % +z up on stage

% actual trajectory
ax   = -data(data_start:data_end,5);
ay   = -data(data_start:data_end,6);
az   =  data(data_start:data_end,7);

% force readouts from load cell
fx   =  data(data_start:data_end,8);  % 
fy   =  data(data_start:data_end,9);
fz   =  data(data_start:data_end,10);
mx   =  data(data_start:data_end,11);
my   =  data(data_start:data_end,12);
mz   =  data(data_start:data_end,13);
