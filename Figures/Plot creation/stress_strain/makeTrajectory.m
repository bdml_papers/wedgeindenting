% 25 July 2008 17:20:40 PDT
% makeTrajectory.m
% this file creates a simple trajectory for the santos limit surface test


% path is specified by vertices in 4 space
% x, y, z, t
% microcontroller creates samples in between

% STANFORD stage coordinates:
% -x is in preload direction
% +y is in transverse direction (don't care)
% +z is in drag direction

% LANL stage coordinates:
% +x is in transverse direction (don't care)
% -y is in drag direction
% +z is in preload direction



%------------------------------------------------------------------
% begin config info
%------------------------------------------------------------------

% configuration variables are loaded in external script
% this is so they can be shared with parseExperiment.m
run ./configure.m;

%------------------------------------------------------------------
% end config info
%------------------------------------------------------------------
mkdir(trajectoryDirectory);
mkdir(dataDirectory);
mkdir(plotDirectory);

batchFID = fopen('batchFile.txt','w+');
totalTime = 0;
for i = 1:length(distancePreloadMicron)
	for j = 1:length(anglePulloffDegree)
		for k = 1:numTrials
				
			% assemble file ID for trajectory file
			trajectoryString = sprintf(trajFormatString,...
			                           distancePreloadMicron(i),...
				                         anglePulloffDegree(j),k);
			trajectoryFID = fopen(fullfile(trajectoryDirectory,trajectoryString),'w+');
			
			% write out filename of trajectory and data file to batch file
			testString = sprintf(testFormatString,...
			                     distancePreloadMicron(i),...
			                     anglePulloffDegree(j),k);
			fprintf(batchFID,'%s\t%s\r\n',fullfile(trajectoryDirectory,trajectoryString),fullfile(dataDirectory,testString));
			
			% vertex 0 home/tare position 
			xMicron = [0,0,0];
			tSecond = 0;
			writeVertexMicron(trajectoryFID,xMicron(1),xMicron(2),xMicron(3),tSecond);
			
			% vertex 1
			% this allows for initial zero force data
			% tare length is currently 1 second
			% this is used in the parseExperiment script
			xMicron = [0,0,0];
			tSecond = tSecond + timeTareSecond;
			writeVertexMicron(trajectoryFID,xMicron(1),xMicron(2),xMicron(3),tSecond);
			
			% vertex 2
			% bottom of preload
			xMoveMicron = [0,...
				              -tan(anglePreloadRadian)*distancePreloadMicron(i),...
				              distancePreloadMicron(i)];

			xMicron = xMicron + xMoveMicron;
			tSecond = tSecond + norm(xMoveMicron) / velocityPreloadMicronPerSecond;
			writeVertexMicron(trajectoryFID, xMicron(1), xMicron(2), xMicron(3), tSecond);
			
			% vertex 3
			% top of pulloff
			xMoveMicron = [0,...
				             -sin(anglePulloffRadian(j)) * distancePulloffMicron,...
				             -cos(anglePulloffRadian(j)) * distancePulloffMicron];
			xMicron = xMicron + xMoveMicron;
			tSecond = tSecond + norm(xMoveMicron) / velocityPulloffMicronPerSecond;
			writeVertexMicron(trajectoryFID, xMicron(1), xMicron(2), xMicron(3), tSecond);
			
			% vertex 4 end of vertical retraction
			xMoveMicron = [0,0,(-distanceRetractionMicron - xMicron(3))];
			xMicron = xMicron + xMoveMicron;
			tSecond = tSecond +... 
								norm(xMoveMicron) / velocityRetractionMicronPerSecond;
			writeVertexMicron(trajectoryFID, xMicron(1), xMicron(2), xMicron(3), tSecond);

			% vertex 5 back to home
			xMoveMicron = [0,0,0] - xMicron;
			xMicron = [0,0,0];
			tSecond = tSecond + norm(xMoveMicron) / velocityHomeMicronPerSecond;
			writeVertexMicron(trajectoryFID, xMicron(1), xMicron(2), xMicron(3), tSecond);

			% vertex 6 settling time
			xMoveMicron = [0,0,0] - xMicron;
			xMicron = [0,0,0];
			tSecond = tSecond + timeSettleSecond;
			writeVertexMicron(trajectoryFID, xMicron(1), xMicron(2), xMicron(3), tSecond);

			% calculation of stage time needed for all tests in run
			totalTime = totalTime + tSecond;

			% vertex 7 zero flag for microcontroller
			xMicron = [0,0,0];
			tSecond = 0;
			writeVertexMicron(trajectoryFID, xMicron(1), xMicron(2), xMicron(3), tSecond);

			fclose(trajectoryFID);
		end
	end
end

fclose(batchFID);

fprintf('Estimated Total Test Time : %4.1f seconds\n',totalTime);
