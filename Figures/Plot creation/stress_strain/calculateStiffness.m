% 31 July 2008 17:03:45 PDT
% calculateStiffness.m
% creates graph of preload force vs preload depth from a set of 
% parsed data

clear all
close all
run ./configure.m

tic

% preallocating arrays for efficiency.  doesn't seem to do anything.
forceXMaxPreload = zeros(length(distancePreloadMicron),...
                         length(anglePulloffDegree),...
                         numTrials);
forceYMaxPreload = forceXMaxPreload;
forceZMaxPreload = forceYMaxPreload;
depthAtMaxPreloadMicron = forceYMaxPreload;

index = 0;
for i = 1:length(distancePreloadMicron)
	for j = 1:length(anglePulloffDegree)
		for k = 1:numTrials
				
			% Load data
			dataFilename = sprintf(testFormatString,... 
			distancePreloadMicron(i), anglePulloffDegree(j), k);
			data = load(dataFilename);
			
			fprintf('Analyzing : %s  ',dataFilename);
			
			[time Dx Dy Dz Ax Ay Az Fx Fy Fz Mx My Mz] = getTraces ( data );
			
			% desired positions
			DxMicron = Dx * 1000;
			DyMicron = Dy * 1000;
			DzMicron = Dz * 1000;

			% actual positions
			AxMicron = Ax * 1000;
			AyMicron = Ay * 1000;
			AzMicron = Az * 1000;
			
			% each of the above are column vectors
			% force is in units of Newtons
			forceXRaw = Fx;
			forceYRaw = Fy;
			forceZRaw = Fz;
			
			% Get vertex indices
			% find takes the logical expression and returns the index of the 
			% first occurence in the array that satisfies the expression
			indexEndTare = find( ( time >= timeTareSecond ), 1, 'first' );
			
			% Tare data using initial idle time
			biasX = mean ( forceXRaw( 1:indexEndTare ));
			biasY = mean ( forceYRaw( 1:indexEndTare ));
			biasZ = mean ( forceZRaw( 1:indexEndTare ));
			
			% takes the mean along the column direction and returns a row vector
			forceXTared = forceXRaw - biasX;
			forceYTared = forceYRaw - biasY;
			forceZTared = forceZRaw - biasZ;	
			
			% Filter data
			[numd,dend] = butter(3,.1);
			forceXFiltered = filtfilt(numd,dend,forceXTared);
			forceYFiltered = filtfilt(numd,dend,forceYTared);
			forceZFiltered = filtfilt(numd,dend,forceZTared);
			
			% Get pre-load
			[depthAtMaxPreloadMicron(i,j,k),sampleMaxPreload] = min( AzMicron );
			forceXMaxPreload(i,j,k) = forceXFiltered(sampleMaxPreload);
			forceYMaxPreload(i,j,k) = forceYFiltered(sampleMaxPreload);						forceZMaxPreload(i,j,k) = forceZFiltered(sampleMaxPreload);
			positionError(i,j,k) = abs(AzMicron(sampleMaxPreload) - ...
																 DzMicron(sampleMaxPreload));
			
			% assemble easily plottable vector of pressures and depths
			index = index + 1;
			pressureMaxPreloadKPa(index) = - forceZMaxPreload(i,j,k) / ...
																  areaSampleSquareCentimeter * 10;
			depthMaxPreload(index) = - depthAtMaxPreloadMicron(i,j,k);
			
			% output text status to console
			fprintf('Depth : %3.3f  ', depthMaxPreload(index));
			fprintf('Preload : %3.3f  ', pressureMaxPreloadKPa(index));	
			fprintf('Error : %3.3f  ',positionError(i,j,k));
			fprintf('\n');

		end
	end
end

% calculate slope from this bidness
P = polyfit(depthMaxPreload, pressureMaxPreloadKPa,1);
slopeString = sprintf('Slope : %3.3f  ', P(1) );
fprintf(slopeString);
fitX = distancePreloadMicron;
fitY = polyval(P, fitX);

% plot fit line
plot(fitX, fitY, 'g-');
hold on;

% plot preload data traces
plot(depthMaxPreload, pressureMaxPreloadKPa, 'k*');

% plot formatting
xlabel('Preload Depth (micron)');
ylabel('Preload Pressure (kPa)');
titleString = sprintf('Preload Stiffness of %s\n', nameSample);
title(titleString);
x = get(gca,'XLim');
y = get(gca,'YLim');
textPositionX = (x(1)+x(2))*0.3;
textPositionY = (y(1)+y(2))*0.8;
text(textPositionX,textPositionY,slopeString,...
				'FontName','Times New Roman',...
				'FontSize',18);
formatPlot(gcf,gca,'Times New Roman',24);
plotFilename = sprintf('preloadStiffness_%s',nameSample);
printPlot(gcf,plotFilename,8,6);


toc
