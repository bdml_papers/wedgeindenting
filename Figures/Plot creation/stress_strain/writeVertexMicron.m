%---------------------------------------%
function writeVertexMicron(trajectoryFID,x,y,z,t)
	% function takes position and time in units of microns and seconds
	% and writes to file converting to millimeters
	% units for microcontroller are in mm
	% we convert by dividing by 1000
	x = x / 1000;
	y = y / 1000;
	z = z / 1000;
	fprintf(trajectoryFID,'%+09.5f\t%+09.5f\t%+09.5f\t%+09.5f\r\n',t,x,y,z);
end
%---------------------------------------%