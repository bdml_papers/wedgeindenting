%function plot_handle = plotForceTrace ( filename, figureHandle, axesHandle );

filename = 'traj_compressive_test_2200um_logfile.txt'

figureHandle = gcf;
axesHandle = gca;

% Correcting for instrument compliance

%Noise floor after which to correct
noise_floor = 0.1; %N

%Sample measurements
d = 9; %mm
h = 0.454*25.4; %mm

sample_area  = (pi*(d/2)^2)/(1e6) %m^2;

% hex_tick = [0x96B 0x975]
tick = [2411 2421]; % 10000 ticks = 1mm

%compression in mm
comp_mm = tick/10000;
    
%compressive force at each above displacement
comp_force = [231.3 231.6]; % N

% Calculate stage stiffness by dividing average force of average
% displacement
stage_K = ((comp_force(1) + comp_force(2))/2)/((comp_mm(1) + comp_mm(2))/2); %N/mm

%stage compliance is inverse of stage stiffness
stage_C = 1/stage_K; % mm/N


data = load ( filename );
[time dx dy dz ax ay az fx fy fz mx my mz] = getTraces(data);

% Tare force data
fxBias = mean(fx(1:1000));
fyBias = mean(fy(1:1000));
fzBias = mean(fz(1:1000));
fx = fx - fxBias;
fy = fy - fyBias;
fz = fz - fzBias;

% tare actual position data
axBias = mean(ax(1:1000));
ayBias = mean(ay(1:1000));
azBias = mean(az(1:1000));
ax = ax - axBias;
ay = ay - ayBias;
az = az - azBias; 

% Switching the sign of our Z actual displacement
%az = -1*az;

% Filter data
[numd,dend] = butter(3,.02);
fx_filt = filtfilt(numd,dend,fx);
fy_filt = filtfilt(numd,dend,fy);
fz_filt = filtfilt(numd,dend,fz);

%Switching the sign of our filtered z force
fz_filt = -1*fz_filt;

% subtract out stage compliance
% Real Z compression = measured z compression - (force * stage_C)

cz = az - (stage_C)*fz_filt;

% for i=1:length(az)
%     if abs(fz(i)>noise_floor)
%     az(i) = az(i) - (abs(fz(i))*stage_C); 
%     end
% end

%spit out the fz_max 
fz_max = max(abs(fz_filt))

% scale force data for plotting purposes
% forceMax = max([fx_filt;fy_filt;fz_filt]);
% forceMin = min([fx_filt;fy_filt;fz_filt]);
% forceScaleFactor = max(abs(forceMax),abs(forceMin));
% fx_filt = fx_filt / forceScaleFactor;
% fy_filt = fy_filt / forceScaleFactor;
% fz_filt = fz_filt / forceScaleFactor;

% scale position data for plotting purposes
% positionMax = max([ax;ay;az]);
% positionMin = min([ax;ay;az]);
% positionScaleFactor = max(abs(positionMax),abs(positionMin));
% ax = ax / positionScaleFactor;
% ay = ay / positionScaleFactor;
% az = az / positionScaleFactor;

% plotting code
axes(axesHandle);
plot(fx_filt,'k');
hold on;
plot(fy_filt,'r');
plot(fz_filt,'b.');
% plot(ax,'k','LineWidth',3);
% plot(ay,'r','LineWidth',3);
% plot(az,'b','LineWidth',3);
legendStrings = {'Fx','Fy','Fz'};
legend(legendStrings,'FontSize',12,'Location','EastOutside');
title(axesHandle, 'Time Trace of Force');
xlabel (axesHandle, 'Time (ms)');
ylabel (axesHandle, 'Newtons');
hold off;
formatPlot( figureHandle, axesHandle, 'Times New Roman', 24 );

figure 

%PLotting desired z, actual z, and corrected z 
plot(dz,'b')
hold on
plot(az,'r')
plot(cz,'g')
title('Desired, Actual, and Corrected Position vs. Time')
legendStrings = {'Dz','Az','Cz'};
legend(legendStrings,'FontSize',12,'Location','EastOutside');


figure

%plot force displacement curve
plot(cz,fz_filt,'b.')
title('Compressive force vs. corrected displacement');

%Divide by sample area to get stress
stress = (fz_filt/sample_area);
strain = cz/h;

%% plot compressive stress train curve

plot(strain,stress,'b.')
hold on
title('Compressive stress vs. corrected strain');
[r,m,b] = regression(strain(1470:2885)',stress(1470:2885)')
%plotregression(strain(1470:3844)',stress(1470:3844)')

E = m*strain(1470:2885) + b;
plot(strain(1470:2885),E,'r.')



