function plot_handle = plotForceTrace ( filename, figureHandle, axesHandle );

% Daniel Soto
% Mon Oct 29 11:44:55 PDT 2007
% <tag> dsoto.matlab </tag>

data = load ( filename );
[time dx dy dz ax ay az fx fy fz mx my mz] = getTraces(data);

% Tare force data
fxBias = mean(fx(1:1000));
fyBias = mean(fy(1:1000));
fzBias = mean(fz(1:1000));
fx = fx - fxBias;
fy = fy - fyBias;
fz = fz - fzBias;

% tare actual position data
axBias = mean(ax(1:1000));
ayBias = mean(ay(1:1000));
azBias = mean(az(1:1000));
ax = ax - axBias;
ay = ay - ayBias;
az = az - azBias;

% Filter data
[numd,dend] = butter(3,.02);
fx_filt = filtfilt(numd,dend,fx);
fy_filt = filtfilt(numd,dend,fy);
fz_filt = filtfilt(numd,dend,fz);

% scale force data for plotting purposes
forceMax = max([fx_filt;fy_filt;fz_filt]);
forceMin = min([fx_filt;fy_filt;fz_filt]);
forceScaleFactor = max(abs(forceMax),abs(forceMin));
fx_filt = fx_filt / forceScaleFactor;
fy_filt = fy_filt / forceScaleFactor;
fz_filt = fz_filt / forceScaleFactor;

% scale position data for plotting purposes
positionMax = max([ax;ay;az]);
positionMin = min([ax;ay;az]);
positionScaleFactor = max(abs(positionMax),abs(positionMin));
ax = ax / positionScaleFactor;
ay = ay / positionScaleFactor;
az = az / positionScaleFactor;

% plotting code
axes(axesHandle);
plot(fx_filt,'k');
hold on;
plot(fy_filt,'r');
plot(fz_filt,'b');
plot(ax,'k','LineWidth',3);
plot(ay,'r','LineWidth',3);
plot(az,'b','LineWidth',3);
legendStrings = {'Fx','Fy','Fz','Ax','Ay','Az'};
legend(legendStrings,'FontSize',12,'Location','EastOutside');
title(axesHandle, 'Time Trace of Force and Position');
xlabel (axesHandle, 'Time (ms)');
ylabel (axesHandle, 'Arb Units');

hold off;
formatPlot( figureHandle, axesHandle, 'Times New Roman', 24 );
