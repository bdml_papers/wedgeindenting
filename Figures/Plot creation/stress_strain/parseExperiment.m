% 25 July 2008 22:39:40 PDT
% parseExperiment.m
% code to parse limit surface test using the min power method

% Stanford force plate and direction coordinates
% x - transverse direction
% y - shear direction
% z - preload direction

% LANL force plate and direction coordinates
% x - transverse direction
% y - shear direction
% z - preload direction


clear all
close all

% start timer for total calculation time
tic

run ./configure.m

FxPulloffAll = [];
FyPulloffAll = [];
FzPulloffAll = [];

currentMax = 0;

for i = 1 : length(distancePreloadMicron)
	for j = 1 : length(anglePulloffDegree)
  	for k = 1 : numTrials;
			% Get trajectory file
			string = sprintf(trajFormatString,...
											 distancePreloadMicron(i),...
											 anglePulloffDegree(j),k);
			trajData = load(fullfile(trajectoryDirectory,string));
	
			t0 = trajData(1,1);
			tStart = trajData(2,1);
			tPreload = trajData(3,1);
			tPulloff = trajData(4,1);
			tClearance = trajData(5,1);
			tZero = trajData(6,1);
			tEnd = trajData(7,1);

			% Load data
			dataFilename = sprintf(testFormatString,...
									 distancePreloadMicron(i),...
									 anglePulloffDegree(j),k);
                                 
			data = load(fullfile(dataDirectory,dataFilename));
			fprintf('Analyzing File : %s\n',dataFilename);


			% Extract data
			time = data(:,1);
			Dx = -data(:,3);
			Dy = -data(:,4);
			Dz = data(:,2);
			Ax = -data(:,6);
			Ay = -data(:,7);
			Az = data(:,5);
			Fx = data(:,8);
			Fy = -data(:,9);
			Fz = data(:,10);

			% Tare data using initial idle time
			FxBias = mean(Fx(1:1000));
			FyBias = mean(Fy(1:1000));
			FzBias = mean(Fz(1:1000));
			Fx = Fx - FxBias;
			Fy = Fy - FyBias;
			Fz = Fz - FzBias;

			% Filter data
			[numd,dend] = butter(3,.02);
			Fx_filt = filtfilt(numd,dend,Fx);
			Fy_filt = filtfilt(numd,dend,Fy);
			Fz_filt = filtfilt(numd,dend,Fz);

			% Get transition index
			index0 = find((time>=t0),1,'first');
			indexStart = find((time>=tStart),1,'first');
			indexPreload = find((time>=tPreload),1,'first');
			indexPulloff = find((time>=tPulloff),1,'first');
			indexClearance = find((time>=tClearance),1,'first');
			indexZero = find((time>=tZero),1,'first');
			indexEnd = find((time>=tEnd),1,'first');

			direction = Az(indexPreload+1:indexPulloff+1)-...
									Az(indexPreload:indexPulloff);
			zSwitchIndex = indexPreload + find(direction>0,1,'first') - 1;
			
			zPlaneIndex = indexPreload + find(Az(indexPreload:indexPulloff)>.2,1,'first') - 1;
			
			% Get pre-load
			[FzPreload(i,j,k),tempIndex] = min(Fz_filt(indexStart:indexPreload));
			samplePreload = tempIndex+indexStart-1;
			FxPreload(i,j,k) = Fx_filt(samplePreload);
			FyPreload(i,j,k) = Fy_filt(samplePreload);

			% Get pull-off or drag
			[FzPulloff(i,j,k),tempIndex] = max(Fz_filt(indexPreload:indexPulloff));
			samplePulloff = tempIndex + indexPreload - 1;
			FxPulloff(i,j,k) = Fx_filt(samplePulloff);
			FyPulloff(i,j,k) = Fy_filt(samplePulloff);

			% Get all forces during pull-off
			FxTemp = Fx_filt(indexPreload:1:indexPulloff);
			FyTemp = Fy_filt(indexPreload:1:indexPulloff);
			FzTemp = Fz_filt(indexPreload:1:indexPulloff);
			FxPulloffAll(i,j,k,1:length(FxTemp)) = FxTemp;
			FyPulloffAll(i,j,k,1:length(FxTemp)) = FyTemp;
			FzPulloffAll(i,j,k,1:length(FxTemp)) = FzTemp;
			
			% Get pull-off via maximum vector between switch and plane
			v = [0;0;1];
			R1 = [1 0 0;... 
						0 cos(anglePulloffRadian(j)) -sin(anglePulloffRadian(j));... 
						0 sin(anglePulloffRadian(j))  cos(anglePulloffRadian(j))];
			v = R1 * v;
			% velocity vector of glass plate
			v = [0; -sin(anglePulloffRadian(j)); cos(anglePulloffRadian(j))];
			% force vector is which force plate or reaction or what?
			Fvector = [Fx_filt(indexPreload:indexPulloff)...
			           Fy_filt(indexPreload:indexPulloff)...
			           Fz_filt(indexPreload:indexPulloff)];
			clear FvectorNorm;
			for index=1:size(Fvector,1)
					FvectorNorm(index) = dot(v,Fvector(index,:));
			end
			[garbage,tempIndex] = max(FvectorNorm);
			samplePulloffVector = tempIndex + indexPreload - 1;
			FxPulloffVector(i,j,k) = Fx_filt(samplePulloffVector);
			FyPulloffVector(i,j,k) = Fy_filt(samplePulloffVector);
			FzPulloffVector(i,j,k) = Fz_filt(samplePulloffVector);
            
           %pick out maximum adhesion 
            if FzPulloffVector(i,j,k) >= currentMax
                FzMax = FzPulloffVector(i,j,k);
                currentMax = FzMax;
                maxI = i;
                maxJ = j;
                maxK = k;
            end
            
            % Get maximum and average positioning error
			Ex = Dx - Ax;
			Ey = Dy - Ay;
			Ez = Dz - Az;
			Error = sqrt(Ex.^2 + Ey.^2 + Ez.^2);
			ErrorMax(i,j,k) = max( Error );
			ErrorMean(i,j,k) = mean( Error );

  	end
	end
end
%% output

            F_adh_max = FzMax;
            F_shear_at_max_adh = abs(FyPulloffVector(maxI,maxJ,maxK))
            P_adh_max = F_adh_max/(0.01^2) %assume 1cm square sample
            P_shear_at_max_adh = F_shear_at_max_adh/(0.01^2)

save experiment

% end timer and output total calculation time
toc
