% 6 August 2008 17:05:39 PDT
% calculateStiffnessThreshold.m
% objective
% calculate stiffness of arrays
% does so by finding initial contact by a threshold method
% and then finds true preload depth
% this should give a higher accuracy to the data 

clear all
close all
run ./configure.m

tic
% preallocating arrays for efficiency.  doesn't seem to do anything.
forceXMaxPreload = zeros(length(distancePreloadMicron),...
                         length(anglePulloffDegree),...
                         numTrials);
forceYMaxPreload = forceXMaxPreload;
forceZMaxPreload = forceYMaxPreload;
depthAtMaxPreloadMicron = forceYMaxPreload;

index = 0;
for i = 1:length(distancePreloadMicron)
	for j = 1:length(anglePulloffDegree)
		for k = 1:numTrials
				
			% Load data
			dataFilename = sprintf(testFormatString,... 
			distancePreloadMicron(i), anglePulloffDegree(j), k);
			data = load(dataFilename);
			
			fprintf('Analyzing : %s  ',dataFilename);
			
			[time Dx Dy Dz Ax Ay Az Fx Fy Fz Mx My Mz] = getTraces ( data );
			
			% desired positions
			DxMicron = Dx * 1000;
			DyMicron = Dy * 1000;
			DzMicron = Dz * 1000;

			% actual positions
			AxMicron = Ax * 1000;
			AyMicron = Ay * 1000;
			AzMicron = Az * 1000;
			
			% each of the above are column vectors
			% force is in units of Newtons
			forceXRaw = Fx;
			forceYRaw = Fy;
			forceZRaw = Fz;
			
			% Get vertex indices
			% find takes the logical expression and returns the index of the 
			% first occurence in the array that satisfies the expression
			indexEndTare =    find( ( time >= timeTareSecond ), 1, 'first' );			
			
			% Tare data using initial idle time
			biasX = mean ( forceXRaw( 1:indexEndTare ));
			biasY = mean ( forceYRaw( 1:indexEndTare ));
			biasZ = mean ( forceZRaw( 1:indexEndTare ));
			
			margin = 200;
			noiseFloorX = std ( forceXRaw( 1+margin:indexEndTare-margin ));
			noiseFloorY = std ( forceYRaw( 1+margin:indexEndTare-margin ));
			noiseFloorZ = std ( forceZRaw( 1+margin:indexEndTare-margin ));

%			fprintf('Noise XYZ = %3.3f, %3.3f, %3.3f  ', ...
%			        noiseFloorX, noiseFloorY, noiseFloorZ);

			forceXTared = forceXRaw - biasX;
			forceYTared = forceYRaw - biasY;
			forceZTared = forceZRaw - biasZ;	
			
			% Filter data
			[numd,dend] = butter( 3, 0.05 );
			forceXFiltered = filtfilt(numd,dend,forceXTared);
			forceYFiltered = filtfilt(numd,dend,forceYTared);
			forceZFiltered = filtfilt(numd,dend,forceZTared);
			
			noiseFloorX = std ( forceXFiltered( 1+margin:indexEndTare-margin ));
			noiseFloorY = std ( forceYFiltered( 1+margin:indexEndTare-margin ));
			noiseFloorZ = std ( forceZFiltered( 1+margin:indexEndTare-margin ));

			% look for threshold crossing in the z force data
			% use filtered data to find this crossing
			threshold = - 4.0 * noiseFloorZ;
			indexStartSearch = 500;
			indexEndSearch = length ( forceZFiltered );
			indexDeparture = find (forceZFiltered(indexStartSearch:indexEndSearch)...
			                       < threshold, 1, 'first');
			indexDeparture = indexDeparture + indexStartSearch - 1;
			%fprintf('indexDeparture = %d  ', indexDeparture);
			%fprintf('threshold = %3.3f  ', threshold);

			% position of actual contact
			depthAtDeparture = - AzMicron(indexDeparture);
						
			% Get pre-load
			[depthAtMaxPreloadMicron(i,j,k),sampleMaxPreload] = min( AzMicron );
			forceXMaxPreload(i,j,k) = forceXFiltered(sampleMaxPreload);
			forceYMaxPreload(i,j,k) = forceYFiltered(sampleMaxPreload);						forceZMaxPreload(i,j,k) = forceZFiltered(sampleMaxPreload);
			positionError(i,j,k) = abs(AzMicron(sampleMaxPreload) - ...
																 DzMicron(sampleMaxPreload));
			
			% assemble easily plottable vector of pressures and depths
			index = index + 1;
			pressureMaxPreloadKPa(index) = - forceZMaxPreload(i,j,k) / ...
																  areaSampleSquareCentimeter * 10;
			depthMaxPreload(index) = - depthAtMaxPreloadMicron(i,j,k);
			depthMaxPreload(index) = depthMaxPreload(index) - depthAtDeparture;
			
			% output text status to console
			
			fprintf('DepthDepart: %3.3f  ', depthAtDeparture);
			fprintf('DepthPreload : %3.3f  ', depthMaxPreload(index));
			fprintf('Error : %3.3f  ',positionError(i,j,k));
			fprintf('\n');

			% plotting code
			figureHandle = figure;
			axesHandle = gca;
			plot(forceXFiltered,'k');
			hold on;
			plot(forceYFiltered,'r');
			plot(forceZFiltered,'b');
			legendStrings = {'Fx','Fy','Fz'};
			legend(legendStrings,'FontSize',12,'Location','EastOutside');
			title(axesHandle, dataFilename, 'Interpreter','None');
			xlabel (axesHandle, 'Time (ms)');
			ylabel (axesHandle, 'Force unknown units for now');
			% plot point of contact
			plot(indexDeparture,forceZFiltered(indexDeparture),'ko');
			% plot max preload point
			plot(sampleMaxPreload,forceZMaxPreload(i,j,k),'ko');
			
			hold off;
			formatPlot( figureHandle, axesHandle, 'Times New Roman', 24 );
			plotFilename = sprintf(plotFormatString, ...
										 distancePreloadMicron(i), anglePulloffDegree(j), k);
			printPlot( figureHandle, plotFilename, 8, 6);
			close(figureHandle);
		end
	end
end

% calculate slope from this bidness
P = polyfit(depthMaxPreload, pressureMaxPreloadKPa,1);
slopeString = sprintf('Slope : %3.3f  ', P(1) );
fprintf(slopeString);
% need to change this range to reflect actual preload distances
fitX = distancePreloadMicron;
fitY = polyval(P, fitX);

% plot fit line
plot(fitX, fitY, 'g-');
hold on;

% plot preload data traces
plot(depthMaxPreload, pressureMaxPreloadKPa, 'k*');

% plot with calculated preload depth


% plot formatting
xlabel('Preload Depth (micron)');
ylabel('Preload Pressure (kPa)');
titleString = sprintf('Preload Stiffness of %s\n', nameSample);
title(titleString);
x = get(gca,'XLim');
y = get(gca,'YLim');
textPositionX = (x(1)+x(2))*0.3;
textPositionY = (y(1)+y(2))*0.8;
text(textPositionX,textPositionY,slopeString,...
				'FontName','Times New Roman',...
				'FontSize',18);
formatPlot(gcf,gca,'Times New Roman',24);
plotFilename = sprintf('preloadStiffness_%s',nameSample);
printPlot(gcf,plotFilename,8,6);


toc
