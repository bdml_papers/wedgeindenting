close all
clear all
load ./experiment
run ./configure.m


FxPulloffVector_mean = mean(FxPulloffVector,3);
FyPulloffVector_mean = mean(FyPulloffVector,3);
FzPulloffVector_mean = mean(FzPulloffVector,3);

FxPulloffVectorKPa = FxPulloffVector ./ areaSampleSquareCentimeter * 10;
FyPulloffVectorKPa = FyPulloffVector ./ areaSampleSquareCentimeter * 10;
FzPulloffVectorKPa = FzPulloffVector ./ areaSampleSquareCentimeter * 10;

% if using corrected preloadDistances
%distancePreloadMicron = distancePreloadMicron - estimateZero;

% generate plot
figure;
hold on
color = ['b' 'g' 'r' 'c' 'm' 'y' 'k'];
shape = ['o' 'x' '+' '*' 's' 'd' 'v' 'p' 'h' '.'];
for i=1:length(distancePreloadMicron)
	for j=1:length(anglePulloffDegree)
		for k=1:numTrials
			plotH(i,j) = plot(-FyPulloffVectorKPa(i,j,k),...
					         -FzPulloffVectorKPa(i,j,k),...
									 [color(mod(i,length(color))+1),...
									  shape(mod(j,length(shape))+1)],...
									 'MarkerSize', 10);
		end
	end
end

% generate legend
inx = 0;
plotHvec = [];
legendStringCells = {};
for i = 1:length(distancePreloadMicron)
	inx = inx + 1;
	legendStringCells{inx} = sprintf('%03d preload',...
		                                 distancePreloadMicron(i));
	plotHvec = [plotHvec plotH(i,1)];
end

for j=1:length(anglePulloffDegree)
	inx = inx + 1;
	legendStringCells{inx} = sprintf('%02d angle',...
		                                 anglePulloffDegree(j));
	plotHvec = [plotHvec plotH(length(distancePreloadMicron),j)];
end

% format plot
legend(plotHvec, legendStringCells, ...
				'FontSize', 12, ...
				'Location','EastOutside' );
title(experimentName);
xlabel('Shear Pressure (kPa)');
ylabel('Normal Pressure (kPa)');

slopeString = sprintf('Patch Area = %3.3f cm^2 ', areaSampleSquareCentimeter );
text(10,40,slopeString, 'FontSize', 18);


axis([-10 50 -10 50]);
x = get(gca,'XLim');
y = get(gca,'YLim');
grid on;
grid minor;
axis(gca,[x,y]);
line(x,[0 0],[0 0],'Color','k','LineWidth',2);
line([0 0],y,[0 0],'Color','k','LineWidth',2);
formatPlot(gcf,gca,'Times New Roman',24);
plotFilename = sprintf('limitSurface_%s',nameSample);
printPlot(gcf,plotFilename,8,6);
