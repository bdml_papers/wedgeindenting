% configure.m
% file contains configuration information to be read by
% makeTrajectory.m and parseData.m

%------------------------------------------------------------------
% begin config info
%------------------------------------------------------------------

nameSample = 'F6 Post-Gamma 20s uWedge Sample 6 run1';
nameExperiment = 'Limit Surface';
experimentName = 'F6-Post-Gamma-20s-uWedge-sample6-run1-glass';
backingLayerMicron = 384; 
areaSampleSquareCentimeter = 1.0;


% preload information
distancePreloadMicron = [10:10:60];
anglePreloadDegree = 45;
anglePreloadRadian = anglePreloadDegree * pi / 180;
velocityPreloadMicronPerSecond = 500;

% pulloff information
anglePulloffDegree = [0:10:90];
anglePulloffRadian = anglePulloffDegree .* pi ./ 180;
distancePulloffMicron = 300;
velocityPulloffMicronPerSecond = 500;

% retraction information
velocityRetractionMicronPerSecond = 500;
distanceRetractionMicron = 300;

% homing information
velocityHomeMicronPerSecond = 500;

timeTareSecond = 1;
timeSettleSecond = 0.5;

numTrials = 3;

trajectoryDirectory = 'traj_glass_sample_run1';
dataDirectory = 'test_glass_sample_run1';
plotDirectory = 'plot_glass_sample_run1';

% formatting strings for creating individual filenames
trajFormatString = 'traj_%03dum_%02ddeg_t%01d.txt';
testFormatString = 'test_%03dum_%02ddeg_t%01d.txt';
plotFormatString = 'plot_%03dum_%02ddeg_t%01d';


filenameDataSummaryText = 'data.txt';
filenameDataSummaryMatlab = sprintf('%s.mat',experimentName);

%------------------------------------------------------------------
% end config info
%------------------------------------------------------------------
