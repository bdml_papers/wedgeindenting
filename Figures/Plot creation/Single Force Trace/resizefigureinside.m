% This script will resize the inner plot box to match the outer window
% size, if you have a plot with axis equal and resize the figure manually.

set(gca,'PlotBoxAspectRatioMode','auto')
set(gca,'DataAspectRatioMode','auto')
axis equal
