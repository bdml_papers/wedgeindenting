% Micro-Wedge Machining for the Manufacture of Directional Dry Adhesives
% Paul Day, Eric Eason, Noe Esparza, David Christensen, Mark Cutkosky
% ASME Journal of Micro and Nano-Manufacturing
% July 3, 2012


\documentclass[onecolumn,twoside,9pt]{asme2e}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{cite}
\usepackage{url}
\usepackage{enumerate}
\usepackage{color}
\usepackage{textcomp}
\usepackage[colorlinks=true,linkcolor=blue,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{setspace}
\usepackage{flafter}

\usepackage{fancyhdr}

% For better-looking vector notation
\let\oldhat\hat
\renewcommand{\vec}[1]{\mathbf{#1}}
\renewcommand{\hat}[1]{\oldhat{\mathbf{#1}}}

% For upright mu symbols in micrometers
\newcommand{\micron}{\hbox{\textmu m}}

% For resubmission to the editor
\usepackage{soul}
% TO HIGHLIGHT TEXT: use \hl{this is some highlighted text}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}

\title{Micro-Wedge Machining for the Manufacture of Directional Dry Adhesives}
\journalname{Journal of Micro and Nano-Manufacturing}

\author{Paul Day\\
	\affiliation{Department of Mechanical Engineering,\\
	Stanford University,\\
	Stanford, CA 94305;\\
	and Los Alamos National Labs,\\
	Los Alamos, NM 87544\\
	e-mail: pday@stanford.edu}
}

\author{Eric V. Eason\\
	\affiliation{Department of Applied Physics,\\
	Stanford University,\\
	Stanford, CA 94305\\
	e-mail: easone@stanford.edu}
}

\author{Noe Esparza\\
	\affiliation{Department of Mechanical Engineering,\\
	Stanford University,\\
	Stanford, CA 94305;\\
	e-mail: noe.esparza@stanford.edu}
}

\author{David Christensen\\
	\affiliation{Department of Mechanical Engineering,\\
	Stanford University,\\
	Stanford, CA 94305;\\
	e-mail: davidc10@stanford.edu}
}

\author{Mark Cutkosky\\
	\affiliation{Department of Mechanical Engineering,\\
Stanford University,\\
Stanford, CA 94305\\
e-mail: cutkosky@stanford.edu}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\abstract{
	\it Directional dry adhesives are inspired by animals such as geckos and are a particularly useful technology for climbing applications. Previously, they have generally been manufactured using photolithographic processes. This paper presents a micro-machining process that involves making cuts in a soft material using a sharp, lubricated tool to create closely spaced negative cavities of a desired shape. The machined material becomes a mold into which an elastomer is cast to create the directional adhesive. The trajectory of the tool can be varied to avoid plastic flow of the mold material that may adversely affect adjacent cavities. The relationship between tool trajectory and resulting cavity shape is established through modeling and process characterization experiments. This micro-machining process is much less expensive than previous photolithographic processes used to create similar features and allows greater flexibility with respect to the micro-scale feature geometry, mold size, and mold material. The micro-machining process produces controllable, directional adhesives, where the normal adhesion increases with shear loading in a preferred direction. This is verified by multi-axis force testing on a flat glass substrate. Upon application of a post-treatment to decrease the roughness of the engaging surfaces of the features after casting, the adhesives significantly outperform comparable directional adhesives made from a photolithographic mold.
	\\
	\\ Keywords: directional, adhesion, gecko, manufacturing, micro-machining
}

\maketitle
\doublespacing
\pagestyle{myheadings} 
%\thispagestyle{fancy}
\cfoot{Test footer}

%\input{Introduction}

%\input{Modeling}
%\input{Fabrication}
%\input{Results}
%\input{Discussion}
%\input{Conclusion}

\begin{acknowledgment}
	The authors thank Rob Dickerson of the Materials Science Lab at the Los Alamos National Laboratory for assistance in characterizing the surface roughness of the features. This work was supported in part by a National Science Foundation NIRT 0708367 and by DARPA DSO.  E. V. Eason is supported by a Hertz Foundation Fellowship, an ABB Stanford Graduate Fellowship, and a National Science Foundation Fellowship.
\end{acknowledgment}

\bibliographystyle{asmejournal}
\bibliography{WedgeIndenting}


% Additional inclusions for final submission
\clearpage

\textbf{List of Captions}
\newline

		\textbf{Figure 1} - SEM micrographs of PDMS directional adhesive features:
		(\emph{a})~unloaded micro-wedges from a photolithographic mold,
		(\emph{b})~micro-wedges under shear loading, and
		(\emph{c})~micro-wedges from a micro-machined mold.
		\newline
		
		\textbf{Figure 2} - A diagram illustrating the geometry and the parameters of the micro-machining process for a single cavity. ``Traj.'' is the tool trajectory; ``S.P.'' is the shear plane.
		\newline

		\textbf{Figure 3} - Surface comparison of PDMS micro-wedges cast from micro-machined wax molds, using
		(\emph{a}) no lubricant,
		(\emph{b}) liquid soap lubricant, and
		(\emph{c}) liquid soap lubricant and ``inking'' post-treatment. A broken wedge, illustrating the wedges' tapered profile, can be seen on the right.
		\newline
		
		\textbf{Figure 4} - Diagram illustrating the steps of the post-treatment process (see text for details)
		\newline
		
		\textbf{Figure 5} - Cross-section of microtome blade used in the micro-machining process, showing its three different beveled sections
		\newline
		
		\textbf{Figure 6} - (\emph{a})\ Micrograph of a single mold cavity created in cutting force tests ($\theta=$ 50\textdegree{}, $t=$ 100~\micron{}) showing triangular built-up region.
		(\emph{b})\ Measured shear stress at the shear plane $f_s / A$ versus $\theta$, with values of shear yield stress $k$ for comparison. T, S, and C correspond to trajectories parallel to the tertiary bevel, secondary bevel, and centerline of the tool. The estimated measurement uncertainties of $\theta$ and $f_s / A$ are 0.6\textdegree{} and 0.8~MPa.
		\newline
		
		\textbf{Figure 7} - Micrographs showing the effect of trajectory angle on mold cavity shape. For some trajectories~(\emph{a--b}), a continuous chip of built-up material is formed after the final cavity. The chip disappears for $\theta \ge$ 56\textdegree{}~(\emph{c--d}).
		\newline
		
		\textbf{Figure 8} - Geometric data taken from characterization experiment micrographs (Fig.~\ref{fig:deformation-micrographs}). Feature height is measured normal to the mold surface from the tip of the cavity to its upper edge.
		\newline
		
		\textbf{Figure 9} - (\emph{a}) Diagram of the adhesive testing apparatus, showing the normal and shear directions.
		(\emph{b}) Diagram illustrating the movement of the positioning stage during load-pull tests.
		\newline
		
		\textbf{Figure 10} - Comparison of the limit curves for macroscopic arrays of adhesive micro-wedges produced with micro-machined molds and photolithographic molds. The measurement uncertainty of the data is estimated to be 2~kPa in normal and 1~kPa in shear.

\clearpage

\begin{figure}
	\centering
	\includegraphics[width=3.34in]{Figures/wedges.pdf}
	\caption{
		fig1.eps
		}
	\label{fig:wedges}
\end{figure}

\clearpage

\begin{figure}[t]
	\centering
	\includegraphics[width=3.34in]{Figures/machining-diagram.pdf}
	\caption{
		fig2.eps
		}
	\label{fig:machining-diagram}
\end{figure}

\clearpage

\begin{figure}[tb]
	\centering
	\includegraphics[width=3.34in]{Figures/surface-finish.pdf}
	\caption{
		fig3.eps
		}
	\label{fig:surface-finish}
\end{figure}

\clearpage

\begin{figure*}[t]
	\centering
	\includegraphics[width=4.80in]{Figures/post-treatment-diagram.pdf}
	\caption{
		fig4.eps
		}
	\label{fig:post-treatment-diagram}
\end{figure*}

\clearpage

\begin{figure*}[t]
	\centering
	\includegraphics[width=5.14in]{Figures/blade-cross-section.pdf}
	\caption{
		fig5.eps
		}
	\label{fig:blade-cross-section}
\end{figure*}

\clearpage

\begin{figure*}
	\centering
	\includegraphics[width=5.48in]{Figures/cutting-forces.pdf}
	\caption{
		fig6.eps}
	\label{fig:cutting-forces}
\end{figure*}

\clearpage

\begin{figure}[tb]
	\centering
	\includegraphics[width=2.67in]{Figures/deformation-micrographs.pdf}
	\caption{
		fig7.eps
		}
	\label{fig:deformation-micrographs}
\end{figure}

\clearpage

\begin{figure}[t]
	\centering
	\includegraphics[width=3.34in]{Figures/deformation-vs-trajectory.pdf}
	\caption{
		fig8.eps
		}
	\label{fig:deformation-vs-trajectory}
\end{figure}

\clearpage

\begin{figure*}[t]
	\centering
	\includegraphics[width=5.48in]{Figures/load-pull-test.pdf}
	\caption{
		fig9.eps
		}
	\label{fig:load-pull-test}
\end{figure*}

\clearpage

\begin{figure*}[t]
	\centering
	\includegraphics[width=4.80in]{Figures/post-treated-limit-curve.pdf}
	\caption{
		fig10.eps
		}
	\label{fig:post-treated-limit-curve}
\end{figure*}

\clearpage


\end{document}
