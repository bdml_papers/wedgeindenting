\section{Fabrication}
\label{sec:fabrication}

\subsection{Materials.}

	The mold fabrication method relies on a few key components to be effective.  Most important is the wedge-shaped tool, whose shape strongly influences the shape of the resulting mold cavities. The tool used here is a PTFE-coated steel disposable microtome blade (Delaware Diamond Knives D554X). This tool has a fine surface finish, with blade roughness on length scales~$\ll1$~\micron{}, an internal angle of $2\beta \approx$~24\textdegree{}, and an edge radius of less than 0.9~\micron{} (see Sec.~\ref{sec:tool-geometry}). 

	The material used for the mold must also be selected for desirable properties. An ideal material for machining would have a homogeneous composition, a relatively low yield strength, and perfect rigid-plastic behavior to minimize elastic recovery of the machined region. As noted by Hirst and Howse~\cite{Hirst1969}, rigid-plastic behavior is most likely to occur in indenting processes if the included angle of the tool is acute and the ratio of Young's modulus to yield stress~$E/Y$ is large.

	In the present case, a soft, rolled sheet wax (Kindt-Collins Master Regular Sheet Wax) is used, having a ratio of Young's modulus to yield stress of approximately $E/Y\approx$~110--160. For this value of $E/Y$ with a tool angle of 24\textdegree{}, according to the results presented by Hirst and Howse, the deformation behavior is not dominated by elastic effects and rigid-plastic behavior may be possible.

	Adhesive wear at the tool-mold interface is undesirable and could lead to a poor surface finish. However, this is mitigated by lubricating the interface. In addition a post-treating process has been devised to refinish the surfaces entirely (Sec.~\ref{sec:post-treatment}). For these reasons the tribological properties of the mold material are not a major concern for material selection.

\subsection{Micro-Machining Method.}

	This micro-machining process may be performed on a standard CNC milling machine or other machine with positioning control in at least two axes and sufficient accuracy. Adhesives have been produced using a tabletop CNC milling machine with 1~\micron{} accuracy (Levil WL400), a larger CNC milling machine with 2.5~\micron{} accuracy (Haas VF-0E), and a motorized stage with an estimated accuracy of 10~\micron{} (Velmex MAXY4009C-S4 and Newport GTS30V). For micro-machining, however, the repeatability over small areas is of greater relevance than the accuracy over the entire workspace of the machine.

	Ultimately, the dimensions of an adhesive patch are constrained only by the width of the microtome blade and the length of the workspace of the machine. With the equipment described above, it is possible to make a single uninterrupted patch of adhesive as large as 76~mm wide by 762~mm long.

	First, the wax is melted and cast into a block to improve the consistency of its plastic behavior and to obtain a desirable form factor for fixturing, and then it is cooled to room temperature. The mold surface is milled and planed to ensure it is flat and parallel to the machine ways. Next, the surface is cleaned and the micro-machining tool is mounted to the machine head. The blade is fixed so that its centerline is tilted by a constant angle of $\lambda=$ 60\textdegree{} with respect to the horizontal surface of the wax (see Fig.~\ref{fig:machining-diagram}). The tip of the blade is then aligned to the wax surface.

\subsubsection{Tool Trajectory.}

	The tool is moved by the machine along a specified 2-D trajectory into the wax until its tip reaches a desired depth~$t$ in the negative $y$-direction (see Fig.~\ref{fig:machining-diagram}). At this point the tool is retracted above the surface and then advanced a set distance in the positive $x$-direction to create a space between cuts. The cycle then repeats.

	The tool trajectory may be chosen from a large space of possible paths. Varying the trajectory provides freedom to control the completed cavity shape and the plastic flow of the mold material. Attempts to characterize the indenting process as a function of the trajectory are described in Sec.~\ref{sec:empirical-characterization}.

\subsubsection{Lubrication.}
\label{sec:lubrication}

	Without lubrication, adhesive wear occurs between the tool and the mold material. SEM examination of the micro-wedges cast from these molds indicates significant surface roughness on critical areas such as the engaging faces that will ultimately generate adhesion (see Fig.~\ref{fig:surface-finish}a). To address this issue, a lubricant has been added to the process to inhibit material transfer from the wax mold to the tool. Several fluids were tested, including various mixtures of water and surfactants in the form of liquid dish soaps. The resulting surfaces were characterized using SEM 3D stereo microscopy, as described in Sec.~\ref{sec:surface-characterization}. The lowest roughness was obtained with a 10:2 concentration of Ajax liquid dish soap (Colgate-Palmolive) to water. This improvement in surface finish can be seen in Fig.~\ref{fig:surface-finish}b.

\begin{figure}[tb]
	\centering
	\includegraphics[width=3.34in]{Figures/surface-finish.pdf}
	\caption{
		Surface comparison of PDMS micro-wedges cast from micro-machined wax molds, using
		(\emph{a}) no lubricant,
		(\emph{b}) liquid soap lubricant, and
		(\emph{c}) liquid soap lubricant and ``inking'' post-treatment. A broken wedge, illustrating the wedges' tapered profile, can be seen on the right.
		}
	\label{fig:surface-finish}
\end{figure}

\begin{figure*}[t]
	\centering
	\includegraphics[width=4.80in]{Figures/post-treatment-diagram.pdf}
	\caption{
		Diagram illustrating the steps of the post-treatment process (see text for details)
		}
	\label{fig:post-treatment-diagram}
\end{figure*}

\subsection{Casting.}

	The completed mold is cleaned with solvents and water to remove all traces of lubricant. A PDMS silicone elastomer (Dow Corning Sylgard~170) is vacuum de-gassed and poured into the mold. For samples for adhesion force testing, a 300~\micron{} thick backing layer of PDMS is desired. This can be achieved by spinning the mold at 160~RPM for 30 seconds, or alternatively a two-part mold may be created by placing a flat sheet of glass upon 300~\micron{} supports which rest on the wax mold surface. For climbing applications~\cite{Hawkes2012}, the sheet of glass may be replaced by a rigid tile made of glass fiber or aluminum. The tile is treated with a primer (Dow Corning PR-1200), which allows the PDMS to bond directly to the tile. In any case, the casting is then allowed to cure at room temperature for 24 hours (heat acceleration is not suitable due to thermal expansion of the wax). Once removed from the mold, the elastomeric adhesive is ready for use. The mold may become damaged as the castings are de-molded, in which case the mold may be resurfaced and micromachined again before its next use (see discussion in Sec.~\ref{sec:discussion}).

\subsection{Post-treatment.}
\label{sec:post-treatment}

	While the addition of lubrication to the micro-machining process improves the surface finish of the molds and molded wedges, there is still remaining roughness that affects the performance of the adhesives by reducing the real area of contact between the adhesive and the substrate. In order to further reduce this roughness, a post-treatment is employed after casting. This treatment adds a thin, smooth secondary layer of PDMS to the engaging faces of the wedges. The treatment proceeds as follows (see Fig.~\ref{fig:post-treatment-diagram}):

\begin{enumerate}
	\item Uncured PDMS is diluted to a concentration of 10$\%$ toluene by volume.  The diluted mixture is then poured onto a four-inch quartz wafer and spun at 8000 RPM for 60 seconds to obtain a uniform thin layer 3--5~\micron{} thick.
	\item One half of the wafer is cleaned using isopropyl alcohol, and the wafer and a cast adhesive sample are secured to a three axis motorized positioning stage (described in Sec.~\ref{sec:adhesive-tests}). The adhesive sample is aligned using the positioning stage's two-axis goniometer.
	\item The sample is brought into contact with the PDMS-coated half of the wafer. After applying a normal load so that the wedges are in contact with the wafer over approximately one third of their length, the adhesive is taken out of contact, leaving a thin, wet layer of PDMS on the tips of the wedges.
	\item After this ``inking'' procedure, the adhesive is loaded against the cleaned half of the wafer and held there in order to flatten this thin, wet layer as it cures.
	\item The cured thin layer binds strongly to the previously cured wedges. The post-treatment results in smooth patches of PDMS on the engaging faces of the wedges (see Fig.~\ref{fig:surface-finish}c).
\end{enumerate}

	Although the thin layer of PDMS deposited on the tips of the wedges is smooth, the surface roughness of the wedges in the subjacent region near the bottom of the wedges appears to be adversely affected by post-treatment (this roughness is visible in Fig.~\ref{fig:surface-finish}c). However, this region does not contact the substrate unless the adhesive is subjected to extreme loads.

\begin{figure*}[t]
	\centering
	\includegraphics[width=5.14in]{Figures/blade-cross-section.pdf}
	\caption{
		Cross-section of microtome blade used in the micro-machining process, showing its three different beveled sections
		}
	\label{fig:blade-cross-section}
\end{figure*}

	For a climbing adhesive which has been cast directly to a rigid tile, the post-treatment may be done without the motorized positioning stage, by simply using an appropriately sized weight. In this variation of the process, the wafer is placed on a flat surface, the adhesive is placed on the wafer (with the back of the tile facing up), and the weight is placed on the tile. This passive alignment technique is more effective than using a rigid positioning stage for large adhesive tiles, because it becomes increasingly difficult to actively align the surfaces as their size increases. Using weights instead of a positioning stage, angular alignment within~0.03\textdegree{} has been achieved (less than 20~\micron{} of height misalignment over a 44~mm length of adhesive). The best post-treatment results have been obtained using weights such that the average pressure is approximately 7--8~kPa, but this depends on the shape and stiffness of the wedges.
